<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;
use App\Models\User;
// use Illuminate\Foundation\Auth\User;

class UserController extends Controller
{
    public function index()
    {
        $data['users'] = DB::table('users')
             ->orderBy('id','desc')
            ->paginate(config('app.row'));
            return view('user.index',$data);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
            //    $data = array(
            //     'name' => $r->name,
            //     'email'=> $r->email,
            //     'photo'=> $r->photo,
            //     'password' => bcrypt($r->password)

            //    );


            //    if($r->photo)
            //    {
            //     $data['photo']=$r->file('photo')->store('uploads/user','custom');
            //    }
            //    $i = DB::table('users')->insert($data);
            //    if($i)
            //    {
            //         $r->session()->flash('Success','Data has been saved!');
            //         return redirect('user/create');
            //    }
            //    else
            //    {
            //         $r->session()->flash('error','Fail to save date!');
            //         return redirect('user.create')->withInput();
            //    }
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->password);
        if($request->hasfile('photo'))
        {

             $file = $request->file('photo');
             $extention = $file->getClientOriginalExtension();
             $filename = time().'.'.$extention;
             $file->move(public_path('uploads/users/'),$filename);
             $user->photo = $filename;
            // $imageName = time().'.'.$request->photo->getClientOriginalExtension();
            // $file = $request->photo->move(public_path('assets/images'), $imageName);
            //  $request->photo->move(public_path('assets/images'), $imageName);
           
        }

                //fix
                    // $imageName = time().'.'.$request->photo->getClientOriginalExtension();
                    // $request->photo->move(public_path('assets/images'), $imageName);
                
                
                    // $user->slug = $validateData['slug'];
                    // $Article->description  = $validateData['description'];
                    // $Article->save();
                //fix


        $result = $user->save();

        if($result){
            return redirect()->route('user.show');
        }
        else{
            return back();
        }
    
    }

    public function show()
    {
        // $users = DB::table('users')->get();
        
        // foreach ($users as $user)
        // {
        //     var_dump($user->name);
        // }
        $users = user::all();
        return view('user.show',compact('users')); 
        
    }
    public function delete($id, Request $r)
    {
        DB::table('users')
        ->where('id',$id)
        ->delete();
        $r->session()->flash('success','Data has been removed!');
        return redirect()->route('user.show');
    }

    public function edit($id)
    {
        $data['user'] = DB::table('users')
        ->where('id',$id)
        ->first();
        return view('user.edit', $data);
    }
    /* update */

    public function update(Request $r)
    {
        $validateData = $r->validate([
            'name'=>'required|min:8',
            'email'=>'required'
        ]);
        
        $data = array(
            'name' => $r->name,
            'email'=>$r->email
        );

        if($r->password!="")
        {
            $data['password'] = bcrypt($r->password);
        }

        $i = DB::table('users')
        ->where('id',$r->id)
        ->update($data);
        if($i)
        {
            $r->session()->flash('success', 'Data has been saved!');
            return redirect('user/show');
        }
        else{
            $r->session()->flash('error','Fail to save data!');
            return redirect('user/edit/'.$r->id);
        }
    }

    public function user()
    {
        Auth::logout();
        // log the user out of our application
       return redirect()->route('login');
    }

}
