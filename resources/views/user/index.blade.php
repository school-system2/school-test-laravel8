@extends('layouts.master')
@section('header')
 @endsection
@section('section')
    <!-- Contains page content -->
    <div class="table-user-account">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <div class="toolbox">
                                <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm btn-oval">
                                    <i class="fas fa-plus-circle"></i> Create
                                </a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Profile</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Create</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td><img src="{{ asset('uploads/users/' . $user->photo) }}" height="30px"
                                                    width="30px"></td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->created_at }}</td>

                                            <td class="jsgrid-cell jsgrid-align-center" style="width: 100px;">
                                                <a href="{{ route('user.delete', $user->id) }}" class="text-danger"
                                                    title="Dalete" onclick="return confirm('You want to delete?')">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                {{-- <tfoot>
                                    <tr>
                                        <th>Rendering engine</th>
                                        <th>Browser</th>
                                        <th>Platform(s)</th>
                                        <th>Engine version</th>
                                        <th>CSS grade</th>
                                    </tr>
                                </tfoot> --}}
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.content-wrapper -->






{{-- test --}}
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Create</th>
            <th>Action</th>
            
        </tr>
    </thead>
    <tbody>
       
        @forelse ($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->photo}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->created_at}}</td>
            <td style="width: 100px;">
                <a href="{{ route('user.delete', $user->id) }}" class="text-danger"
                    title="Dalete" onclick="return confirm('You want to delete?')">
                    <i class="fas fa-trash-alt"></i>
                </a>
            </td>
        </tr>
        @empty
            <tr>
                <td colspan="5">No information found.</td>
            </tr>
        @endforelse 
       
     
    </tbody>
   
</table>
    
@endsection
