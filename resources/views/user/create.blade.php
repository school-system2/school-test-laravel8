@extends('layouts.master')
@section('header')
    <strong>Create User</strong>
@endsection
@section('section')
    <section class="content">
        <form action="{{ route('user.save') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @if (Session::has('success'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <p>{{ session('success') }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <p>{{ session('error') }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">

                    <div class="toolbox">
                        <button type="submit">
                            <i class="fas fa-save"></i> Save
                        </button>
                        <a href="{{ url('/user/show') }}" class="btn btn-warning btn-sm btn-oval">
                            <i class="fas fa-reply"></i> Back
                        </a>
                    </div>



                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label for="name" class="col-ns-3">Username
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <input name="name" type="text" class="farm-control float-right w-100" id="name"
                                        required autofocus value="{{ old('name')?? ''}}">
                                    {{-- {{ $r->name ?? '' }} --}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-ns-3">Email
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <input type="email" class="farm-control float-right w-100" id="email"
                                        name="email" required value="{{ old('email') ?? ''}}">
                                    {{-- {{!! $r->email !!}} --}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-ns-3">Password
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <input type="password" class="farm-control float-right w-100" id="password"
                                        name="password" required>
                                    {{-- {{!! $password->password !!}} --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">

                            <div class="form-group row">
                                <label for="photo" class="col-ns-3">Photo
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <input type="file" class="farm-control" id="photo" name="photo" onchange="preview(event)"
                                        accept="image/png, image/gif, image/jpeg" required autofocus>
                                        <p class="mt-4">
                                            <img src="" alt="" width="120" id="img">
                                        </p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </form>
    </section>
@endsection

@section('script')
<script>
    function preview(evt){
        let img = document.getElementById('img');
        img.src = URL.createObjectURL(evt.target.files[0]);
    }
</script>
@endsection