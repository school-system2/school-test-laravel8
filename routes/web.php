<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Route::get('/',"DashboardController@index");



// or
// route user

Route::get('/user/logout',[UserController::class,'user'])->name('user.logout');

Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

Route::group(['middleware'=>'auth'],function()
{
    Route::view('/test','layouts.master')->name('test');

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/user',[UserController::class, 'index'])->name('user.table');

    Route::get('/user/delete/{id}',[UserController::class, 'delete'])->name('user.delete');

    Route::get('/user/edit/{id}',[UserController::class, 'edit'])->name('user.edit');

    Route::get('/user/create',[UserController::class, 'create'])->name('user.create');

    Route::post('/user/save',[UserController::class, 'store'])->name('user.save');

    Route::post('/user/update',[UserController::class, 'update'])->name('user.update');

    Route::get('/user/show',[UserController::class, 'show'])->name('user.show');


}) ;

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
